const success = data => Promise.resolve(data);
const error = data => Promise.reject(data);

const probe = message => data => {
  console.log(message, data);
  return data;
};

success('Kevin Heritage')
  .then(probe('First then'))
  .then(data => success(success(success(success(data)))))
  .then(probe('Third then'))
  .then(data => success(success(success(error(data)))))
  .then(probe('We wont hit this'))
  .catch(probe('We hit this instead'))
  .then(probe('We can hit this too'));
